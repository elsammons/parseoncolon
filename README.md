# parseoncolon

## Description
Parse a string passed in the form of key:value.

## Example
```
from parseoncolon import parseoncolon

aString = "key1:value1 key2:value2 key3:this could be a value for key3"

d = parseoncolon(aString)
print(d)
```
> {'key3': 'this could be a value for key3', 'key2': 'value2', 'key1': 'value1'}
